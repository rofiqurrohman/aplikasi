﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_DataAccess_Library
{
    public abstract class Entity
    {
        public abstract string Id { get; set; }
    }

    public abstract class Repository<T> where T : Entity
    {
        private DataVerbs _verb;

        protected abstract List<T> List { get; set; }

        public abstract T Model { get; set; }

        public string DataSerialized {
            get {
                _verb = DataVerbs.Get;
                return JsonConvert.SerializeObject(Read());
            }
            set {
                _verb = (DataVerbs)Convert.ToInt32(value);
                switch (_verb)
                {
                    case DataVerbs.Post:
                        Create(Model);
                        break;
                    case DataVerbs.Put:
                        Update(Model, Model.Id);
                        break;
                    case DataVerbs.Delete:
                        Delete(Model.Id);
                        break;
                }
            }
        }

        private bool Create(T data)
        {
            List.Add(data);
            return true;
        }

        private IEnumerable<T> Read()
        {
            return List;
        }

        private T Read(string id)
        {
            return List.Where((T arg) => arg.Id == id).FirstOrDefault();
        }

        private bool Update(T data, string id)
        {
            List.Remove(Read(id));
            List.Add(data);
            return true;
        }

        private bool Delete(string id)
        {
            List.Remove(Read(id));
            return true;
        }
    }
}
