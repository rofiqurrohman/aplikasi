﻿using System.Collections.Generic;
using RPL_DataAccess_Library.Data;

namespace RPL_DataAccess_Library.Repositories {
    public class OrdersDetailRepository : Repository<OrdersDetail> {
        private List<OrdersDetail> _list;

        protected override List<OrdersDetail> List {
            get {
                if (_list == null) {
                    _list = new List<OrdersDetail>() {
                        new OrdersDetail() { Id = "1", Orders = { Id = "1" }, Qty = "1", Amount = "150000" },
                        new OrdersDetail() { Id = "2", Orders = { Id = "2" }, Qty = "1", Amount = "200000" },
                    };
                }
                return _list;
            }
            set {
                _list = value;
            }
        }

        public override OrdersDetail Model { get; set; }
    }
}
