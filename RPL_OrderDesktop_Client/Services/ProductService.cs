﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using RPL_DataAccess_Library.Data;
using RPL_DataAccess_Library.Repositories;
using RPL_DataAccess_Library;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.Services {
    public class ProductService : IServices<ProductModel> {
        public ObservableCollection<ProductModel> ItemData { get; set; } = new ObservableCollection<ProductModel>();
        public List<ProductModel> Items { get; set; } = new List<ProductModel>();
                
        private readonly Repository<Product> repo = new ProductRepository();
        private readonly ProductModel model = new ProductModel();

        public async Task<bool> Event_ReadAsync(List<ProductModel> item) {
            try {
                foreach (var value in JArray.Parse(repo.DataSerialized)) {
                    var json = JObject.Parse(value.ToString());
                    item.Add(model.Entity(json));
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            Items = item;
            return await Task.FromResult(true);
        }

        public async Task<bool> Event_ReadAsync() {
            Items = new List<ProductModel>();

            try {
                foreach (var value in JArray.Parse(repo.DataSerialized)) {
                    var json = JObject.Parse(value.ToString());
                    Items.Add(model.Entity(json));
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            } finally {
                ItemData.Clear();
                foreach (var item in Items) {
                    ItemData.Add(item);
                }
            }
            return await Task.FromResult(true);
        }

        public async Task Event_CreateAsync(ProductModel model) {
            repo.Model = model;
            repo.DataSerialized = ((int)DataVerbs.Post).ToString();
            await Event_ReadAsync();
        }

        public async Task Event_UpdateAsync(ProductModel model) {
            repo.Model = model;
            repo.DataSerialized = ((int)DataVerbs.Put).ToString();
            await Event_ReadAsync();
        }

        public async Task Event_DeleteAsync(ProductModel model) {
            var msg = MessageBox.Show("Are You Sure?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.Yes) {
                repo.Model = model;
                repo.DataSerialized = ((int)DataVerbs.Delete).ToString();
                await Event_ReadAsync();
            }
        }
    }
}
