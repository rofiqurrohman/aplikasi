﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using RPL_OrdersDesktop_Client.Services;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.ViewModels {
    public class BaseViewModel : INotifyPropertyChanged {
        protected IServices<OrdersModel> ServOrders = new OrdersService();
        protected IServices<ProductModel> ServProduct = new ProductService();

        private string _title = string.Empty;

        public string Title {
            get {
                return _title;
            }
            set {
                SetProperty(ref _title, value);
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            var changed = PropertyChanged;

            if (changed == null) return;
            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T backingStore, T value, [CallerMemberName]string propertyName = "", Action onChanged = null) {
            if (EqualityComparer<T>.Default.Equals(backingStore, value)) {
                return false;
            }

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
