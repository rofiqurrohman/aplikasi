﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

using RPL_OrdersDesktop_Client.Setup;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.ViewModels {
    internal delegate Task<bool> CallDataOrdersTask(List<OrdersModel> item);

    public class OrdersViewModel : BaseViewModel {
        private ObservableCollection<OrdersModel> itemdata;
        private OrdersModel model;

        public OrdersViewModel() {
            model = new OrdersModel();
            itemdata = ServOrders.ItemData;

            SaveCommand = new CommandSetup(async () => {
                await ServOrders.Event_CreateAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            UpdateCommand = new CommandSetup(async () => {
                await ServOrders.Event_UpdateAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            DeleteCommand = new CommandSetup(async () => {
                await ServOrders.Event_DeleteAsync(Model);
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            ResetCommand = new CommandSetup(async () => {
                await ServOrders.Event_ReadAsync();
                OnPushComponent?.Invoke(this, new PushComponentEvent());
            });

            var task = new CallDataOrdersTask(ServOrders.Event_ReadAsync);
            var asyncprogres = task.BeginInvoke(ServOrders.Items, new AsyncCallback(LoadDataCallback), null);
            task.EndInvoke(asyncprogres);

            Title = "Form Orders List";
        }

        public OrdersModel Model {
            get {
                return model;
            }
            set {
                SetProperty(ref model, value);
            }
        }

        public ObservableCollection<OrdersModel> ItemData {
            get {
                return itemdata;
            }
            set {
                SetProperty(ref itemdata, value);
            }
        }

        public ICommand SaveCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ResetCommand { get; set; }

        private void LoadDataCallback(IAsyncResult result) {
            Task.Factory.StartNew(() => {
                Application.Current.Dispatcher.Invoke(delegate {
                    ItemData.Clear();
                    foreach (var item in ServOrders.Items) {
                        ItemData.Add(item);
                    }
                });
            });
        }

        public event PushComponent OnPushComponent;
    }
}
