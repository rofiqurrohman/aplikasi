﻿using System.Windows;
using System.Windows.Controls;

namespace RPL_OrdersDesktop_Client.Views.Main {
    public partial class MainMenuView : UserControl {
        public MainMenuView() {
            InitializeComponent();
        }

        private void BtnMenuOrders_Click(object sender, RoutedEventArgs e) {
            //TabViewControl("Orders", false);
            //TabViewControl("Orders", true, new Orders.OrdersView());
        }

        private void BtnMenuProduct_Click(object sender, RoutedEventArgs e) {
            new Product.ProductView().ShowDialog();
        }

        private void BtnMenuSysInfo_Click(object sender, RoutedEventArgs e) {

        }
    }
}
